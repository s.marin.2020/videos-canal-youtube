# Videos Canal Youtube

Ejercicio 13.5

Se pide descargar el fichero RSS con los videos del canal CursosWeb de Youtube (https://www.youtube.com/channel/UC300utwSVAYOoRLEqmsprfg), y construir un programa que produzca como salida sus títulos en una página HTML. Si se carga esa página en un navegador,  picando sobre un titular, el navegador deberá cargar la página de Youtube con el video correspondiente. Como base puede usarse lo aprendido estudiando el programa xml-parser-jokes.py que hemos visto en clase.

En el programa de la asignatura podrás encontrar más información sobre este ejercicio.

